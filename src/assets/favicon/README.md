# Your Favicon Package

This package was generated with [RealFaviconGenerator](https://realfavicongenerator.net/) [v0.16](https://realfavicongenerator.net/change_log#v0.16)

## Install instructions

To install this package:

Extract this package in <code>&lt;web site&gt;/assets/favicon/</code>. If your site is <code>http://www.example.com</code>, you should be able to access a file named <code>http://www.example.com/assets/favicon/favicon.ico</code>.

Insert the following code in the `head` section of your pages:

    <link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png?v=Kmnx6w0N7E">
    <link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png?v=Kmnx6w0N7E">
    <link rel="icon" type="image/png" sizes="194x194" href="/assets/favicon/favicon-194x194.png?v=Kmnx6w0N7E">
    <link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-chrome-192x192.png?v=Kmnx6w0N7E">
    <link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png?v=Kmnx6w0N7E">
    <link rel="manifest" href="/assets/favicon/site.webmanifest?v=Kmnx6w0N7E">
    <link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg?v=Kmnx6w0N7E" color="#2d89ef">
    <link rel="shortcut icon" href="/assets/favicon/favicon.ico?v=Kmnx6w0N7E">
    <meta name="apple-mobile-web-app-title" content="OpenBlu">
    <meta name="application-name" content="OpenBlu">
    <meta name="msapplication-TileColor" content="#2d89ef">
    <meta name="msapplication-config" content="/assets/favicon/browserconfig.xml?v=Kmnx6w0N7E">
    <meta name="theme-color" content="#191c20">

*Optional* - Check your favicon with the [favicon checker](https://realfavicongenerator.net/favicon_checker)