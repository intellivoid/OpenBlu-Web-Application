<!-- Generated Content, Reference ID <?PHP print(strtoupper(hash('sha256', time() . 'REF') . '(INTELLIVOID-TECHNOLOGIES)')); ?> -->
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
<link rel="stylesheet" href="/assets/vendors/iconfonts/mdi/css/materialdesignicons.min.css">
<link rel="stylesheet" href="/assets/vendors/iconfonts/font-awesome/css/font-awesome.min.css">
<link rel="stylesheet" href="/assets/vendors/css/vendor.bundle.base.css">
<link rel="stylesheet" href="/assets/vendors/css/vendor.bundle.addons.css">
<link rel="stylesheet" href="/assets/vendors/animate.css/animate.min.css">
<link rel="stylesheet" href="/assets/vendors/iconfonts/flag-icon-css/css/flag-icon.min.css">
<link rel="stylesheet" href="/assets/css/style.css">
<link rel="stylesheet" href="/assets/vendors/morris/morris.css">
<link rel="stylesheet" href="/assets/css/app.css">
<link rel="apple-touch-icon" sizes="180x180" href="/assets/favicon/apple-touch-icon.png?v=Kmnx6w0N7E">
<link rel="icon" type="image/png" sizes="32x32" href="/assets/favicon/favicon-32x32.png?v=Kmnx6w0N7E">
<link rel="icon" type="image/png" sizes="194x194" href="/assets/favicon/favicon-194x194.png?v=Kmnx6w0N7E">
<link rel="icon" type="image/png" sizes="192x192" href="/assets/favicon/android-chrome-192x192.png?v=Kmnx6w0N7E">
<link rel="icon" type="image/png" sizes="16x16" href="/assets/favicon/favicon-16x16.png?v=Kmnx6w0N7E">
<link rel="manifest" href="/assets/favicon/site.webmanifest?v=Kmnx6w0N7E">
<link rel="mask-icon" href="/assets/favicon/safari-pinned-tab.svg?v=Kmnx6w0N7E" color="#2d89ef">
<link rel="shortcut icon" href="/assets/favicon/favicon.ico?v=Kmnx6w0N7E">
<meta name="apple-mobile-web-app-title" content="OpenBlu">
<meta name="application-name" content="OpenBlu">
<meta name="msapplication-TileColor" content="#2d89ef">
<meta name="msapplication-config" content="/assets/favicon/browserconfig.xml?v=Kmnx6w0N7E">
<meta name="theme-color" content="#191c20">
<meta name="title" content="<?PHP \DynamicalWeb\HTML::print(SERVER_META_IP); ?> (<?PHP \DynamicalWeb\HTML::print(SERVER_META_COUNTRY); ?>)">
<meta name="description" content="Free OpenVPN Server from <?PHP \DynamicalWeb\HTML::print(SERVER_META_COUNTRY); ?>. Get more servers from OpenBlu, a Free VPN Service powered by volunteers, get access to over 10,000 OpenVPN Servers to use with any device or server for free">
<meta property="og:type" content="website">
<meta property="og:url" content="https://openblu.intellivoid.net/servers/view?pub_id=<?PHP \DynamicalWeb\HTML::print(SERVER_META_ID); ?>">
<meta property="og:title" content="OpenBlu - <?PHP \DynamicalWeb\HTML::print(SERVER_META_IP); ?> (<?PHP \DynamicalWeb\HTML::print(SERVER_META_COUNTRY); ?>)">
<meta property="og:description" content="Free OpenVPN Server from <?PHP \DynamicalWeb\HTML::print(SERVER_META_COUNTRY); ?>. Get more servers from OpenBlu, a Free VPN Service powered by volunteers, get access to over 10,000 OpenVPN Servers to use with any device or server for free">
<meta property="og:image" content="/assets/images/web_banner.jpg">
<meta property="twitter:card" content="summary_large_image">
<meta property="twitter:url" content="https://openblu.intellivoid.net/servers/view?pub_id=<?PHP \DynamicalWeb\HTML::print(SERVER_META_ID); ?>">
<meta property="twitter:title" content="OpenBlu - <?PHP \DynamicalWeb\HTML::print(SERVER_META_IP); ?> (<?PHP \DynamicalWeb\HTML::print(SERVER_META_COUNTRY); ?>)">
<meta property="twitter:description" content="Free OpenVPN Server from <?PHP \DynamicalWeb\HTML::print(SERVER_META_COUNTRY); ?>. Get more servers from OpenBlu, a Free VPN Service powered by volunteers, get access to over 10,000 OpenVPN Servers to use with any device or server for free">
<meta property="twitter:image" content="/assets/images/web_banner.jpg">